module Orders
    open System

    type Undefined = Exception

    type OrderTime = private OrderTime of DateTime
    type MenuHyperlink = private MenuHyperlink of string
    type DeliveryPrice = private DeliveryPrice of decimal

    type SpecialOfferDetails = SpecialOfferDetails of string

    type SpecialOffer = {
        Details : SpecialOfferDetails
    }

    type NumberOfPeople = private NumberOfPeople of int

    type BankAccount = BankAccount of string
    type Cash = Cash


    [<System.FlagsAttribute>]
    type PaymentMethod =
        | CashPayment of Cash
        | BankAccountPayment of BankAccount

    type UnvalidatedOrder = private {
        OrderTime: OrderTime
        MenuHyperlink : MenuHyperlink
        DeliveryPrice : DeliveryPrice
        SpecialOffer: SpecialOffer
        NumberOfPeopleRestriction: NumberOfPeople option
        PaymentMethod: PaymentMethod
    }

    type PlacedOrder = Undefined

    type AwaitingOrder = Undefined

    type FinishedOrder = Undefined

    type CompletedOrder = Undefined

    type PlaceOrder = UnvalidatedOrder -> PlacedOrder

    type Order =
        | Unvalidated of UnvalidatedOrder
        | Placed of PlacedOrder
        | Awaiting of AwaitingOrder
        | Completed of CompletedOrder
        
    module OderTime =
        let create dateTime =
            if dateTime < DateTime.Now then
                None
            else
                Some <| OrderTime dateTime

    module MenuHyperlink = 
        type Errors =
            | NotWellFormedUrl 
            
        let create hyperlink =
            let x = Uri.IsWellFormedUriString(hyperlink, UriKind.Absolute)
            match x with
            | true -> Result.Ok <| MenuHyperlink hyperlink
            | false -> Result.Error <| NotWellFormedUrl

    module DeliveryPrice =
        type Errors =
            | NegativeDeliveryPrice

        let create deliveryPrice =
            match deliveryPrice with
            | x when x < 0m -> Result.Error <| NegativeDeliveryPrice
            | _ -> Result.Ok <| DeliveryPrice deliveryPrice

    module NumberOfPeople =
        type Errors =
            | NegativeNumberOfPeople
            
        let create number =
            match number with
            | x when x > 0 -> Result.Ok <| NumberOfPeople number
            | _ -> Result.Error <| NegativeNumberOfPeople

    module BankAccount =
        type Errors =
            | InvalidBankNumber
        let create bankAccount =
            match bankAccount with
            | x when String.IsNullOrWhiteSpace x -> Result.Error InvalidBankNumber
            | _ -> Result.Ok <| BankAccount bankAccount

    module UnvalidatedOrder =        
        let create orderTime menuHyperlink deliveryPrice specialOffer 
            numberOfPeopleRestriction paymentMethod =
            {
                OrderTime = orderTime
                MenuHyperlink = menuHyperlink
                DeliveryPrice = deliveryPrice
                SpecialOffer = specialOffer
                NumberOfPeopleRestriction = numberOfPeopleRestriction
                PaymentMethod = paymentMethod
            }
            
            