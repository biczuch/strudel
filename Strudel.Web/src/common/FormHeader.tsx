﻿import { Row, Col } from "antd";
import React from "react";

export interface FormHeaderProps {
    title: string
}

function FormHeader(props: FormHeaderProps) {
    return (<Row>
        <Col span={8} style={{ textAlign: 'end' }}>
            <h2>{props.title}</h2>
        </Col>
    </Row>);
}

export default FormHeader;