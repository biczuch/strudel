﻿import { FormikValues, FormikErrors, FormikTouched, validateYupSchema, FormikProps } from "formik";
import React from "react";
import * as yup from 'yup'
import { CheckboxChangeEvent } from "antd/lib/checkbox";
import { Moment } from "moment";
import { FormError } from "../redux/applicationState";
import { FORMERR } from "dns";

export const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 8 },
    },
};

export const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 16,
            offset: 8,
        },
    },
};

export const nameOfField = <T extends any>() => (name: keyof T) => name.toString()

export const formErrorForField = <T extends FormikValues>(errors: FormikErrors<T>, touched: FormikTouched<T>) => {
    return (name: keyof T) => {
        if (touched[name] && errors[name]) {
            return errors[name];
        }
        return null;
    }
}

export const printFormErrors  = (formErrors: FormError) => {
    if (formErrors.hasError) {
        return (
            <ul>
                {formErrors.errors.map((val) => <li>val</li>)}
            </ul>
        )
    }
    return null;
}

export const hasFormError = <T extends FormikValues>(errors: FormikErrors<T>, touched: FormikTouched<T>) => {
    return (name: keyof T): 'success' | 'warning' | 'error' | 'validating' => {
        if (touched[name] && errors[name]) {
            return "error";
        }
        if (touched[name])
            return "success";

        return null;
    }
}

export const isRequiredForFields = <T extends any>(validationSchema: yup.ObjectSchema<T>, object: (T)) => {
    return (name: keyof T) => {
        let fieldSchema = yup.reach(validationSchema, name.toString(), object);
        const { tests } = fieldSchema.describe()
        const isRequired = tests.some((t: any) => t === "required")
        return isRequired;
    }
}

export enum FormItemType {
    Normal,
    Tail
}

export const defaultFormItemProps = <T extends any>(formikProps: FormikProps<any>, validationSchema: yup.ObjectSchema<T>) => {
    return (name: keyof (T), formItemType: FormItemType = FormItemType.Normal) => {

        const field = yup.reach(validationSchema, name.toString(), formikProps.values);

        let formItemProps : any = {
            required: isRequiredForFields(validationSchema, formikProps.values)(name.toString()),
            hasFeedback: true,
            validateStatus: hasFormError(formikProps.errors, formikProps.touched)(name.toString()),
            help: formErrorForField(formikProps.errors, formikProps.touched)(name.toString()),
        }

        if (formItemType && formItemType == FormItemType.Tail) {
            formItemProps = { ...tailFormItemLayout, ...formItemProps };
        }
        else if (formItemType == FormItemType.Normal) {
            formItemProps = {
                ...formItemLayout,
                ...formItemProps,
                label: field.describe().label ? field.describe().label : name.toString()
            };
        }

        return formItemProps;
    }
}
export const defaultInputProps = <T extends object, F>(formikProps: FormikProps<any>) => {
    return (name: keyof (F)) => {
        return {
            name: nameOfField<F>()(name),
            value: formikProps.values[name.toString()],
            onChange: formikProps.handleChange,
            onBlur: formikProps.handleBlur,
        }
    }
}

export const defaultInputNumberProps = <T extends object, F>(formikProps: FormikProps<any>, validationSchema: yup.ObjectSchema<T>) => {
    return (name: keyof (F)) => {

        const fieldSchema = yup.reach(validationSchema, name.toString(), formikProps.values, null);
        const testsDescriptions = (fieldSchema as any).tests.map((x: any) => x.TEST) as Array<yup.TestOptions>;

        const maxTest = testsDescriptions.find(x => x.name === "max");
        const minTest = testsDescriptions.find(x => x.name === "min");

        const defaults = {
            name: nameOfField<F>()(name),
            value: formikProps.values[name.toString()],
            onChange: (val: any) => formikProps.setFieldValue(name, val),
            onBlur: formikProps.handleBlur,
            className: 'defaultInputNumber',
            required: isRequiredForFields(validationSchema, formikProps.values)(name.toString()),
            label: fieldSchema.describe().label
        }

        let inputNumberProps = defaults as any; 

        if (maxTest) {
            inputNumberProps = { ...inputNumberProps, max: (maxTest.params as any)["max"] }
        }
        if (minTest) {
            inputNumberProps = { ...inputNumberProps, min: (minTest.params as any)["min"] }
        }

        return inputNumberProps;
    }
}

export const defaultCheckboxProps = <T extends any>(formikProps: FormikProps<any>, validationSchema: yup.ObjectSchema<T>) => {
    return (name: keyof (T), useValidationSchemeLabel : boolean = true) => {
        let defaultProps : any = {
            name: nameOfField<T>()(name),
            onChange: (val: CheckboxChangeEvent) => {
                formikProps.setFieldValue(name.toString(), val.target.checked)
            },
            checked: formikProps.values[name.toString()],
        }

        if (useValidationSchemeLabel) {
            const validationField = yup.reach(validationSchema, name.toString(), formikProps.values);
            const description = validationField.describe();
            defaultProps = { ...defaultProps, children: description.label ? description.label : name.toString() }
        }

        return defaultProps;
    }
}

export const defaultDatePickerProps = <T extends any>(formikProps: FormikProps<any>, validationSchema: yup.ObjectSchema<T>) => {
    return (name: keyof (T), useValidationSchemeLabel: boolean = true) => {
        let defaultProps: any = {
            defaultValue: formikProps.values[name.toString()],

            onChange: (val: Moment) => {
                formikProps.setFieldTouched("orderDate", true);
                formikProps.setFieldValue("orderDate", val)
            },
        }
        return defaultProps;
    }
}

export const FormProps = <T extends any>(formikProps: FormikProps<any>, validationSchema: yup.ObjectSchema<T>) => {
    return {
        formItemProps: defaultFormItemProps<T>(formikProps, validationSchema),
        inputProps: defaultInputProps<any, T>(formikProps),
        inputNumberProps: defaultInputNumberProps<any, T>(formikProps, validationSchema),
        checkboxProps: defaultCheckboxProps<T>(formikProps, validationSchema),
        datePickerProps: defaultDatePickerProps(formikProps, validationSchema) 
    }
}