﻿import { FormError } from "../redux/applicationState";
import * as React from 'react'

function FormErrors(props: { formErrors: FormError }) {

    const formErrors = props.formErrors;

    if (formErrors.hasError) {
        return (
            <ul>
                {formErrors.errors.map((val) => <li>val</li>)}
            </ul>
        )
    }
    return null;
}

export default FormErrors;