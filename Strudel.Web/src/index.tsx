﻿import * as React from "react"
import * as ReactDOM from "react-dom"
import { App } from './components/appComponent'
import { Provider } from 'react-redux'
import { store } from './redux/store'
import './app.css'

let rootElement = document.getElementById("root")

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>
    , rootElement
);

if ((module as any).hot) {
    (module as any).hot.accept( App, function () {
        var NextApp = App
        ReactDOM.render(<NextApp />, rootElement)
    })
}