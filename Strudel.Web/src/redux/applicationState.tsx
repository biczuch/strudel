﻿export interface FormError {
    hasError: boolean,
    errors: Array<string>
}

export const noErrors = (): FormError => {
    return {
        hasError: false,
        errors: []
    }
}

export interface FormsState {
    addOrderForm: FormError
}

export interface ApplicationState {
    forms: FormsState,
    test: TestState
}

export interface TestState {
    testString: string
}



export const initialState: ApplicationState = {
    forms: {
        addOrderForm: noErrors()
    },
    test: { testString : "test state" }
}