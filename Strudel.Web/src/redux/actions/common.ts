﻿import { ApplicationState } from '../applicationState'
import { ThunkAction } from 'redux-thunk'
import { Action } from 'redux';


type ThunkResult<Result, Actions extends Action<any>> = ThunkAction<Result, ApplicationState, undefined, Actions>

