﻿import { bindActionCreators, ActionCreator, Action } from 'redux'
import { ThunkAction, ThunkDispatch } from 'redux-thunk'
import { Dispatch } from 'react-redux';
import { ApplicationState } from '../applicationState';
import { disconnect } from 'cluster';

export interface Order {
    place: string,
    orderDate: Date,
    menuHyperLink: string,
    deliveryPrice: number,
    specialOffer: boolean,
    specialOfferDetails: string,
    numberOfPeopleRestriction: number,
    acceptCash: boolean,
    acceptBankTransfer: boolean,
    bankAccount: string
}


export const enum OrderActionTypes {
    ORDER_CREATED = 'ORDER_CREATED',
    ORDER_CREATED_FAILED = 'ORDER_CREATED_FAILED',
    OTHER = 'OTHER'
}

interface OrderCreatedAction extends Action {
    type: OrderActionTypes.ORDER_CREATED,
    order: Order
}

interface OrderCreatedFailedAction extends Action {
    type: OrderActionTypes.ORDER_CREATED_FAILED,
    errors: Array<string>
}

export type KnownActions = OrderCreatedAction | OrderCreatedFailedAction

export type OrderThunkAction<R> = ThunkAction<R, ApplicationState, undefined, KnownActions>

export const actionCreators =
    {
        createOrder: (order: Order): OrderThunkAction<void> => {
            return (dispatch, getState, extraArgument) => {
                setTimeout(function () {
                    dispatch({ type: OrderActionTypes.ORDER_CREATED, order})
                }, 2000)
            }
        }
    }
