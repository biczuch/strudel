﻿import { bindActionCreators, ActionCreator, Action } from 'redux'
import { ThunkAction, ThunkDispatch } from 'redux-thunk'
import { Dispatch } from 'react-redux';
import { ApplicationState } from '../applicationState';
import { disconnect } from 'cluster';

export const enum TestActionsTypes {
    CHANGE_TEST_STRING = 'CHANGE_TEST_STRING',
    OTHER = 'OTHER'
}

interface ChangeTestStringAction extends Action {
    type: TestActionsTypes.CHANGE_TEST_STRING,
    testString: string
}

export type KnownActions = ChangeTestStringAction

export type TestThunkAction<R> = ThunkAction<R, ApplicationState, undefined, KnownActions>

export const actionCreators =
    {
        changeTestString: (testString: string): TestThunkAction<void> => {
            return (dispatch, getState, extraArgument) => {
                dispatch({ type: TestActionsTypes.CHANGE_TEST_STRING, testString: testString })
            }
        }
    }
