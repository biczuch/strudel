﻿import { createStore, combineReducers, applyMiddleware} from 'redux'
import testReducer from './reducers/testReducer'
import formReducer from './reducers/formReducer'
import { initialState, ApplicationState } from './applicationState'
import { composeWithDevTools } from 'redux-devtools-extension'
import reduxThunk from 'redux-thunk'

let enhancers = composeWithDevTools(
    applyMiddleware(reduxThunk)
);

const rootReducer = combineReducers<ApplicationState>({
    test: testReducer,
    forms: formReducer
});

export const store = createStore(rootReducer, initialState, enhancers)