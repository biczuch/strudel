﻿import { FormsState, initialState, noErrors } from '../applicationState'
import { KnownActions, OrderActionTypes } from '../actions/OrderActions'
import { AnyAction } from 'redux';

export default function formReducer(state: FormsState = initialState.forms, action: KnownActions): FormsState {
    switch (action.type) {
        case OrderActionTypes.ORDER_CREATED:
            return { addOrderForm: noErrors() }
        case OrderActionTypes.ORDER_CREATED_FAILED:
            return {
                addOrderForm: {
                    hasError: true,
                    errors: action.errors
                }
            }
        default:
            return state;
    };
}