﻿import { TestState, initialState } from '../applicationState'
import { TestActionsTypes, KnownActions } from '../actions/testActions'
import { AnyAction } from 'redux';

export default function testReducer(state: TestState = initialState.test, action: KnownActions): TestState {
    switch (action.type) {
        case TestActionsTypes.CHANGE_TEST_STRING:
            return { testString: action.testString }
        default:
            return state;
    };
}