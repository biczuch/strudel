﻿import * as React from 'react'
import ReactDOm from 'react-dom'
import moment from 'moment';
import { Form, Input, InputNumber, DatePicker, TimePicker, Col, Row, Checkbox, Layout, Button } from 'antd'
import { FormProps, FormItemType, formItemLayout, tailFormItemLayout, defaultCheckboxProps, printFormErrors } from '../common/formCommon'
import FormHeader from '../common/FormHeader';
import { withFormik, Formik, FormikValues, FormikProps, FormikErrors, FormikActions } from 'formik'
import { schema as validationSchema } from '../validations/addOrderValidation'
import { ValidationError } from 'yup';
import { FormError } from '../redux/applicationState';
import FormErrors from '../common/FormErrors';

const FormItem = Form.Item
const { TextArea } = Input
const { Header, Content, Footer } = Layout

export interface AddOrderFields {
    place: string,
    orderTime: moment.Moment | any,
    orderDate: moment.Moment | any,
    menuHyperLink: string,
    deliveryPrice: number,
    specialOffer: boolean,
    specialOfferDetails: string,
    numberOfPeopleRestriction: number,
    acceptCash: boolean,
    acceptBankTransfer: boolean,
    bankAccount: string
}

type AddOrderFormValues = AddOrderFields & FormikValues

interface AddOrderProps {
    handleSubmit: (values: AddOrderFormValues) => void
    formErrors: FormError
    //handleSubmit: (values: AddOrderFormValues) => Promise<FormikErrors<AddOrderFormValues>>
}

type AddOrderAllProps = FormikProps<AddOrderFormValues> & AddOrderProps

class AddOrder extends React.PureComponent<AddOrderAllProps>
{
    constructor(props: AddOrderAllProps) {
        super(props);
    }

    timeFormat = "HH:mm"
    
    render() {
        const { setFieldValue, isSubmitting, handleChange } = this.props;
        const { checkboxProps, formItemProps, inputNumberProps, inputProps, datePickerProps } = FormProps(this.props, validationSchema);

        return (
            <Row>
                <FormHeader title="Add new order" />
                <Row>
                    <FormErrors formErrors={this.props.formErrors} />
                    <Form layout="horizontal" onSubmit={this.props.handleSubmit}>
                        <FormItem {...formItemProps("place")} >
                            <Input {...inputProps("place")} />
                        </FormItem>
                        <FormItem {...formItemProps("orderDate")}>
                            <DatePicker {...datePickerProps("orderDate")}
                                allowClear={false}
                                disabledDate={(currentDate) => currentDate < moment()} />
                        </FormItem>
                        <FormItem label="Order time" {...formItemProps("orderTime")} >
                            <TimePicker onChange={(val) => {
                                this.props.setFieldTouched("orderTime", true);
                                this.props.setFieldValue("orderTime", val)
                            }} defaultValue={this.props.values.orderTime} format={this.timeFormat} allowEmpty={true} />
                        </FormItem>
                        <FormItem {...formItemProps("menuHyperLink")}  >
                            <Input {...inputProps("menuHyperLink")} />
                        </FormItem>
                        <FormItem {...formItemProps("deliveryPrice")}>
                            <InputNumber {...inputNumberProps("deliveryPrice")} step={0.5} precision={2} defaultValue={0}/>
                        </FormItem>
                        <FormItem {...formItemProps("specialOffer", FormItemType.Tail)}>
                            <Checkbox {...checkboxProps("specialOffer")}/>
                        </FormItem>
                        {
                            this.props.values.specialOffer &&
                            <FormItem {...formItemProps("specialOfferDetails")} >
                                <TextArea {...inputProps("specialOfferDetails")} />
                            </FormItem>
                        }
                        <FormItem {...formItemProps("numberOfPeopleRestriction")} >
                            <InputNumber {...inputNumberProps("numberOfPeopleRestriction")} />
                        </FormItem>
                        <FormItem {...formItemProps("acceptCash", FormItemType.Tail)}>
                            <Checkbox {...checkboxProps("acceptCash")} />
                        </FormItem>
                        <FormItem {...formItemProps("acceptBankTransfer", FormItemType.Tail)} >
                            <Checkbox {...checkboxProps("acceptBankTransfer")}/>
                        </FormItem>
                        {
                            this.props.values.acceptBankTransfer &&
                            <FormItem {...formItemProps("bankAccount")}>
                                <Input {...inputProps("bankAccount")} />
                            </FormItem>
                        }
                        <FormItem {...tailFormItemLayout}>
                            <Button type="primary" htmlType="submit" className="login-form-button" disabled={isSubmitting} loading={isSubmitting}>
                                Submit
                            </Button>
                        </FormItem>
                    </Form>
                </Row>
            </Row>
            //lista ludzi, oczekiwany czas dostawy/delivery time
        )
    }
}

const getPlus1HourTime = (): moment.Moment => {
    let currentDate = moment().toDate();
    currentDate.setHours(currentDate.getHours() + 1);
    return moment(currentDate);
}

const mapPropsToValues = (props: AddOrderProps): AddOrderFormValues => {
    return {
        place: '',
        orderTime: getPlus1HourTime(),
        orderDate: moment(),
        menuHyperLink: '',
        acceptCash: false,
        bankAccount: '',
        deliveryPrice: 0,
        numberOfPeopleRestriction: 0,
        specialOfferDetails: '',
        specialOffer: false,
        acceptBankTransfer: false,
    }
}

const handleSubmit = (values: AddOrderFormValues, formikBag: { props: AddOrderProps } & FormikActions<AddOrderFormValues>): void => {
    formikBag.props.handleSubmit(values);
    setTimeout(() => {
        formikBag.setSubmitting(false)
    }, 2000);
    
}

export const AddOrderForm = withFormik<AddOrderProps, AddOrderFormValues>({
    mapPropsToValues,
    handleSubmit,
    validationSchema
})(AddOrder)