﻿import * as React from 'react'
import { bindActionCreators } from 'redux'
import { connect, Dispatch } from 'react-redux'
import { ApplicationState } from '../redux/applicationState';
import { actionCreators, TestThunkAction } from '../redux/actions/testActions'
import { debug } from 'util';

interface TestProps {
    testString: string
}

interface OwnProps {
    exampleString: string
}

interface ActionProps {
    changeTestString: (testString: string) => TestThunkAction<void>
}

interface TestState {
    testString: string
}

type TestComponentProps = TestProps & OwnProps & ActionProps

class TestComponent extends React.Component<TestComponentProps, TestState> {

    constructor(props: any) {
        super(props, null);
        this.props = props;
        this.state = { testString: this.props.testString };
        this.changeState = this.changeState.bind(this);
        this.submitForm = this.submitForm.bind(this);
    }

    private submitForm() {
        this.props.changeTestString(this.state.testString);
    }

    private changeState(event: React.FormEvent<HTMLInputElement>) {
        this.setState({ testString: event.currentTarget.value })
    }

    render() {
        return (
            <div className="App">
                <h1> Hello, World! </h1>
                <div>{this.props.testString}</div>
                <input type='input' value={this.state.testString} onChange={this.changeState} />
                <input type='button' value='submit' onClick={this.submitForm} />
            </div>
        );
    }
}

const mapStateToProps = (state: ApplicationState, ownProps: OwnProps): TestProps => {
    if (ownProps.exampleString === "example")
        return { testString: "example" }
    else
        return { testString: state.test.testString }
}

function mapDispatchToProps(): ActionProps {
    return {
        changeTestString: actionCreators.changeTestString  
    }
}

export default connect(mapStateToProps, mapDispatchToProps())(TestComponent)