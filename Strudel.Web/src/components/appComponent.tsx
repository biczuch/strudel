﻿import * as React from 'react';
import Test from './testComponent';
import { NavBar } from './navBarComponent';
import { AddOrderForm } from './addOrderComponent'
import { Layout } from 'antd'
import AddOrderContainer from './addOrderContainer';
import TestComponent from './testComponent'

const { Header, Content, Footer } = Layout;

export class App extends React.Component<{}, {}>
{
    render() {
        return (
            <Layout>
                <Header style={{ lineHeight: '64px' }}>
                    <NavBar />
                </Header>

                <Content style={{ padding: '0 50px' }}>
                    <AddOrderContainer />
                </Content>
                <Footer style={{ textAlign: 'center' }}>
                    Strudel @2018 Created by Biczuch
                </Footer>
            </Layout>
        );
    }
}
