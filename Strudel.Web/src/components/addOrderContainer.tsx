﻿import * as React from 'react'
import { connect } from 'react-redux'
import { AddOrderForm, AddOrderFields } from './addOrderComponent'
import { ApplicationState, FormError } from '../redux/applicationState'

import { actionCreators, OrderThunkAction, Order } from '../redux/actions/OrderActions'
import { Moment } from 'moment';
import { stat } from 'fs';

interface AddOrderContainterProps {
    addOrderFormErrors: FormError
}

interface ActionProps {
    createOrder: (order: Order) => OrderThunkAction<void>
}

type AddOrderContainerAllProps = AddOrderContainterProps & ActionProps

class AddOrderContainer extends React.Component<AddOrderContainerAllProps, {}>{

    constructor(props: AddOrderContainerAllProps) {
        super(props);
    }

    submitForm(formValues: AddOrderFields) {

        const orderTime: Moment = (formValues.orderDate as Moment);
        orderTime.set({ hour: 11, minute: 11, second: 0 })

        const order: Order = {
            place: formValues.place,
            orderDate: orderTime.toDate(),
            menuHyperLink: formValues.menuHyperLink,
            deliveryPrice: formValues.deliveryPrice,
            specialOffer: formValues.specialOffer,
            specialOfferDetails: formValues.specialOfferDetails,
            acceptCash: formValues.acceptCash,
            numberOfPeopleRestriction: formValues.numberOfPeopleRestriction,
            acceptBankTransfer: formValues.acceptBankTransfer,
            bankAccount: formValues.bankAccount
        }

        this.props.createOrder(order);
    }

    render() {
        return (
            <AddOrderForm handleSubmit={this.submitForm} formErrors={this.props.addOrderFormErrors} />
        );
    }
}

const mapStateToProps = (state: ApplicationState): AddOrderContainterProps => {
    return {
        addOrderFormErrors: state.forms.addOrderForm
    }
}

const mapDispatchToProps = function (): ActionProps {
    return {
        createOrder: actionCreators.createOrder
    }
}


export default connect(mapStateToProps, mapDispatchToProps())(AddOrderContainer)