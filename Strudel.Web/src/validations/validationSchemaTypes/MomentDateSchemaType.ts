﻿import * as yup from 'yup'
import { isDate } from 'moment';
import * as moment from 'moment'

var DateSchema = yup.date;

export class MomentDateSchemaType extends DateSchema {
    _validFormats: any[];
    constructor() {
        super();
        this._validFormats = [];

        this.withMutation(() => {
            this.transform(function (value, originalvalue) {
                if (this.isType(value))
                    // we have a valid value
                    return value;
                return moment(originalvalue, this._validFormats, true);
            });
        });
    }

    _typeCheck(value: any) {
        return (
            isDate(value) || (moment.isMoment(value) && value.isValid())
        );
    }

    format(formats: any) {
        if (!formats) throw new Error('must enter a valid format');
        let next = this.clone();
        next._validFormats = formats;
        return next;
    }
}