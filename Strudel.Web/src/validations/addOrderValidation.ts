﻿import { AddOrderFields, AddOrderForm } from '../components/addOrderComponent'
import * as yup from 'yup'
import { nameOfField } from '../common/formCommon'
import { validateYupSchema } from 'formik';
import * as moment from 'moment'
import { MomentDateSchemaType } from './validationSchemaTypes/MomentDateSchemaType';

let nameof = nameOfField<AddOrderFields>();

var momentSchema = new MomentDateSchemaType();

export const schema = yup.object<AddOrderFields>().shape({
    place: yup.string().label("Place").max(255).required().trim(),
    orderDate: momentSchema.label("Order date").required().min(new Date(), ),
    orderTime: momentSchema.label("Order time").required().min(new Date()),
    menuHyperLink: yup.string().label("Menu hyperlink").url().required(),
    deliveryPrice: yup.number().label("Delivery price").min(0).max(10).positive(),
    specialOffer: yup.boolean().label("Special offer").required().default(false),
    specialOfferDetails: yup.string().label("Special Offer Details")
        .when(nameof("specialOffer"), {
            is: true,
            then: (s: yup.Schema<string>) => s.required(),
            otherwise: (s: yup.Schema<string>) => s.notRequired()
        }),
    numberOfPeopleRestriction: yup.number().label("Maximum number of people").min(0).integer().positive(),
    acceptCash: yup.boolean().label("Accept cash").required(),
    acceptBankTransfer: yup.boolean().label("Accept bank transfer").required().default(true),
    bankAccount: yup.string().label("Bank account")
        .when(nameof("acceptBankTransfer"), {
            is: true,
            then: (s: yup.Schema<string>) => s.required(),
            otherwise: (s: yup.Schema<string>) => s.notRequired()
        }),
})
