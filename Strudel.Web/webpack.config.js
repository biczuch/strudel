﻿const path = require("path");
const webpack = require("webpack");

module.exports = {
    entry: { main: "./src/index.tsx" },
    mode: "development",
    devtool: "source-map",
    module: {
        rules: [
            {
                test: /\.(js|jsx|tsx|ts)$/,
                exclude: /(node_modules|bower_components)/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            plugins: [["import", { "libraryName": "antd", "libraryDirectory": "es", "style": "css" }]]
                        }
                    },
                    { loader: 'awesome-typescript-loader' }
                ],
                //options: { presets: ['env'] }
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            //{
            //    test: /\.tsx$/,
            //    use: 'awesome-typescript-loader',
            //    exclude: /node_modules/
            //},
            {
                enforce: 'pre',
                test: /\.js$/,
                loader: 'source-map-loader'
            }
        ]
    },
    resolve: { extensions: ['*', '.js', '.jsx', '.tsx', '.ts', '.json'] },
    output: {
        path: path.resolve(__dirname, "wwwroot", 'dist'),
        publicPath: "/dist/",
        filename: "bundle.js"
    },
    devServer: {
        contentBase: path.join(__dirname, "public/"),
        port: 3000,
        publicPath: "http://localhost:3000/dist/",
        hotOnly: true
    },
    plugins: []
};