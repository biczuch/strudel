﻿using Microsoft.AspNetCore.Mvc;

namespace Strudel.Web.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
    }
}
