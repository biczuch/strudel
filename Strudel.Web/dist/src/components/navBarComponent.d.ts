import * as React from 'react';
import { ClickParam } from 'antd/lib/menu';
interface NavBarState {
    current: string;
}
export declare class NavBar extends React.Component<{}, NavBarState> {
    constructor(props: any);
    handleClick: (e: ClickParam) => void;
    render(): JSX.Element;
}
export {};
