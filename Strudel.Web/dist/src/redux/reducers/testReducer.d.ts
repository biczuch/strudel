import { TestState } from '../applicationState';
import { KnownActions } from '../actions/testActions';
export default function testReducer(state: TestState, action: KnownActions): TestState;
