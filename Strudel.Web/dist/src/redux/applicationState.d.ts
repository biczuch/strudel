export interface ApplicationState {
    testState: TestState;
}
export interface TestState {
    testString: string;
}
export declare const initialState: ApplicationState;
